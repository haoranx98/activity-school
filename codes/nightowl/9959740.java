/**
     * 冒泡排序函数
     * aa bb cc
     *
     * @param arr 待排序的数组
     * @param len 待排序的数组长度
     */
    public static void bubbleSort(int[] arr, int len) {
        // 你的代码，使无序数组 a 变得有序
        for (int i = 0; i < len - 1; i++) {
            //对数组进行遍历，相邻的两个数字进行比较，每次比较之后数字总数就会减一防止索引越界，让长度减一
            for (int j = 0; j < len - 1 - i; j++) {
                //判断每两个相邻的数字，大的往前排
                if (arr[j] >= arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    } //end
